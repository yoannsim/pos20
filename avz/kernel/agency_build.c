/*
 * Copyright (C) 2016,2017 Daniel Rossier <daniel.rossier@soo.tech>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <config.h>
#include <percpu.h>
#include <sched.h>
#include <ctype.h>
#include <console.h>
#include <domain.h>
#include <errno.h>
#include <memory.h>
#include <libelf.h>
#include <memslot.h>

#include <asm/processor.h>
#include <asm/mmu.h>

#include <soo/arch-arm.h>

#include <asm/cacheflush.h>

#define L_TEXT_OFFSET	0x8000

start_info_t *agency_start_info;

/*
 * setup_page_table_guestOS() is setting up the 1st-level and 2nd-level page tables within the domain.
 */
int setup_page_table_guestOS(struct domain *d, unsigned long v_start, unsigned long map_size, unsigned long p_start, unsigned long vpt_start) {
	uint32_t vaddr, *new_pt;

	ASSERT(d);

	/* Make sure that the size is 1 MB-aligned */
	map_size = ALIGN_UP(map_size, TTB_SECT_SIZE);

	printk("*** Setup page tables of the domain: ***\n");
	printk("   v_start          : 0x%lx\n", v_start);
	printk("   map size (bytes) : 0x%lx\n", map_size);
	printk("   phys address     : 0x%lx\n", p_start);
	printk("   vpt_start        : 0x%lx\n", vpt_start);

	/* guest page table address (phys addr) */
	d->addrspace.pgtable_paddr = (vpt_start - v_start + p_start);
	d->addrspace.pgtable_vaddr = vpt_start;

	d->addrspace.ttbr0[d->processor] = cpu_get_ttbr0() & ~TTBR0_BASE_ADDR_MASK;
	d->addrspace.ttbr0[d->processor] |= d->addrspace.pgtable_paddr;

	/* Manage the new system page table dedicated to the domain. */
	new_pt = (uint32_t *) __lva(vpt_start - v_start + p_start); /* Ex.: 0xc0c04000 */

	/* copy page table of idle domain to guest domain */
	memcpy(new_pt, __sys_l1pgtable, TTB_L1_SIZE);

	/* Clear the area below the I/Os, but preserve of course the page table itself which is located within the first MB */
	for (vaddr = 0; vaddr < CONFIG_HYPERVISOR_VIRT_ADDR; vaddr += TTB_SECT_SIZE)
		*((uint32_t *) l1pte_offset(new_pt, vaddr)) = 0;

	/* Do the mapping of new domain at its virtual address location */
	create_mapping(new_pt,  v_start, p_start, map_size, false);

	/* We have to change the ptes of the page table in our new page table :-) (currently pointing the hypervisor page table. */
	vaddr = (uint32_t) l1pte_offset(new_pt, vpt_start);

	*((uint32_t *) vaddr) &= ~TTB_L1_SECT_ADDR_MASK; /* Reset the pfn */
	*((uint32_t *) vaddr) |= ((uint32_t ) d->addrspace.pgtable_paddr) & TTB_L1_SECT_ADDR_MASK;

	mmu_page_table_flush((uint32_t) new_pt, ((uint32_t) new_pt) + TTB_L1_SIZE);

	return 0;
}

extern char hypercall_start[];

int construct_agency(struct domain *d) {
	unsigned long vstartinfo_start;
	unsigned long v_start;
	unsigned long alloc_spfn;
	unsigned long vpt_start;
	struct start_info *si = NULL;
	unsigned long nr_pages;

	printk("***************************** Loading SOO Agency Domain *****************************\n");

	/* The agency is always in slot 1 */

	/* Now the slot is busy. */
	memslot[MEMSLOT_AGENCY].busy = true;

	if (memslot[MEMSLOT_AGENCY].size == 0)
		panic("No agency image supplied\n");

	/* The following page will contain start_info information */
	vstartinfo_start = (unsigned long) alloc_heap_page();

	d->max_pages = ~0U;
	d->tot_pages = 0;

	nr_pages = memslot[MEMSLOT_AGENCY].size >> PAGE_SHIFT;

	printk("Max dom size %d\n", memslot[MEMSLOT_AGENCY].size);

	printk("Domain length = %lu pages.\n", nr_pages);

	ASSERT(d);

	d->tot_pages = memslot[MEMSLOT_AGENCY].size >> PAGE_SHIFT;
	alloc_spfn = memslot[MEMSLOT_AGENCY].base_paddr >> PAGE_SHIFT;

	clear_bit(_VPF_down, &d->pause_flags);
	v_start = L_PAGE_OFFSET;

	vpt_start = v_start + TTB_L1_SYS_OFFSET;  /* Location of the system page table (see head.S). */

	/* vstack is used when the guest has not initialized its own stack yet; put right after _end of the guest OS. */

	setup_page_table_guestOS(d, v_start, memslot[MEMSLOT_AGENCY].size, (alloc_spfn << PAGE_SHIFT), vpt_start);

	/* Lets switch to the page table of our new domain - required for sharing page info */

	mmu_switch(&d->addrspace);

	si = (start_info_t *) vstartinfo_start;

	agency_start_info = si;
	memset(si, 0, PAGE_SIZE);

	si->domID = d->domain_id;
	si->nr_pages = d->tot_pages;
	si->min_mfn = alloc_spfn;

	/* Propagate the virtual address of the shared info page for this domain */
	si->shared_info = d->shared_info;

	si->hypercall_addr = (unsigned long) hypercall_start;

	si->fdt_paddr = memslot[MEMSLOT_AGENCY].fdt_paddr;

	printk("Agency FDT device tree: 0x%lx (phys)\n", si->fdt_paddr);

	/* HW details on the CPU: processor ID, cache ID and ARM architecture version */

	si->printch = printch;
	si->pt_base = vpt_start;

	mmu_switch(&current->addrspace);

	d->vstartinfo_start = vstartinfo_start;


	/* Create the first thread associated to this domain. */
	new_thread(d, v_start + L_TEXT_OFFSET, si->fdt_paddr, v_start + memslot[MEMSLOT_AGENCY].size, vstartinfo_start);


	return 0;
}

