/*
 * Copyright (C) 2016-2017 Daniel Rossier <daniel.rossier@soo.tech>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <libfdt/fdt_support.h>

#include <device/fdt.h>

#include <memslot.h>
#include <sched.h>

#include <lib/image.h>

#include <soo/soo.h>
#include <soo/uapi/avz.h>

#include <asm/mmu.h>
#include <asm/cacheflush.h>

/**
 * We put all the guest domains in ELF format on top of memory so
 * that the domain_build will be able to elf-parse and load to their final destination.
 */
void loadAgency(void)
{
	uint32_t dom_addr;
	int nodeoffset;
	uint8_t tmp[16];
	u64 base, size;
	int len;



	dom_addr = fdt_getprop_u32_default((void *) _fdt_addr, "/fit-images/agency", "load-addr", 0);

	/* Set the memslot base address to a section boundary */
	memslot[MEMSLOT_AGENCY].base_paddr = (dom_addr & ~(SZ_1M - 1));
	memslot[MEMSLOT_AGENCY].fdt_paddr = __lpa(_fdt_addr);
	memslot[MEMSLOT_AGENCY].size = fdt_getprop_u32_default((void *) _fdt_addr, "/agency", "domain-size", 0);

	/* Fixup the agency device tree */
	/* find or create "/memory" node. */
	nodeoffset = fdt_find_or_add_subnode((void *) _fdt_addr, 0, "memory");
	BUG_ON(nodeoffset < 0);

	fdt_setprop((void *) _fdt_addr, nodeoffset, "device_type", "memory", sizeof("memory"));

	base = (u64) memslot[MEMSLOT_AGENCY].base_paddr;
	size = (u64) memslot[MEMSLOT_AGENCY].size;

	len = fdt_pack_reg((void *) _fdt_addr, tmp, &base, &size);

	fdt_setprop((void *) _fdt_addr, nodeoffset, "reg", tmp, len);

}


