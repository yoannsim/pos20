/*******************************************************************
 * access.c
 *
 * Copyright (c) 2020 HEIG-VD, REDS Institute
 *******************************************************************/

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/ioport.h>
#include <linux/mod_devicetable.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/irqreturn.h>
#include <linux/gfp.h>
#include <linux/leds.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/string.h>
#include <linux/platform_device.h>
#include <linux/bug.h>
#include <linux/device.h>
#include <linux/input.h>
#include "rpisense.h"


#include <asm/io.h>

#define LED_OFFSET 0x3a
#define SWITCH_OFFSET 0x12
#define SWITCH_IRQ_OFFET 0x18
#define RPI4 = 1;
#define VEXPR = 0;

const char*  ledsName[] = {"vext_led0","vext_led1","vext_led2","vext_led3","vext_led4"};
static int keys[] = {KEY_ENTER,KEY_LEFT,KEY_UP,KEY_RIGHT,KEY_DOWN};

typedef struct {
	struct led_classdev led_cdev;
	int nr_led;
} vext_led_t;


typedef struct{
	int plateforme;
	struct input_dev *input;
	unsigned short *regSwitch;
	unsigned short *switchIrqGlobal;
	unsigned short *ledGlobal;
	vext_led_t leds[5];
} vext_priv_t;








/*########################################## à mettre dans vexperss.c######################################*/

static irqreturn_t  vext_IRQ_handler(int irq, void* dev_id){


	struct platform_device *pdev = container_of((struct device*)dev_id,struct platform_device, dev);

	vext_priv_t *data_priv = (vext_priv_t*) platform_get_drvdata(pdev);


	//clear irq
	*data_priv->switchIrqGlobal = 0x80;


	return IRQ_WAKE_THREAD;
}


static irqreturn_t  vext_IRQ_thread(int irq, void* dev_id){

	unsigned numSwitch = 0;
	struct platform_device *pdev = container_of((struct device*)dev_id,struct platform_device, dev);

	vext_priv_t *data_priv = (vext_priv_t*) platform_get_drvdata(pdev);

	numSwitch = (*data_priv->switchIrqGlobal >> 1) & 0x7;


	if(*data_priv->regSwitch & ((unsigned short) 0x1 << numSwitch)){
		printk("interrupt appui  swich number %d ",numSwitch);


		input_report_key(data_priv->input,keys[numSwitch],1);
		input_sync(data_priv->input);

		input_report_key(data_priv->input,keys[numSwitch],0);
		input_sync(data_priv->input);


	}
	else{
		//printk("interrupt relachement swich pas utilisé pour le moment\n");
	}

	return IRQ_HANDLED;
}


void init_vexpress(struct platform_device *pdev,vext_priv_t *data_priv){

	struct resource *iores;
	int irq;
	int ret;

	iores = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	irq = platform_get_irq(pdev, 0);

	//def le handler d' interrution
	ret = request_threaded_irq(irq, vext_IRQ_handler,vext_IRQ_thread, IRQF_TRIGGER_HIGH, "vext", &pdev->dev);

	printk("access: small driver for accessing I/O ...\n");

	//map de la mem
	data_priv->ledGlobal = (unsigned short*) ioremap(iores->start + LED_OFFSET, iores->end - iores->start);
	data_priv->switchIrqGlobal = (unsigned short*) ioremap(iores->start + SWITCH_IRQ_OFFET, iores->end - iores->start);
	data_priv->regSwitch = (unsigned short*) ioremap(iores->start + SWITCH_OFFSET, iores->end - iores->start);

	//enable IRQ
	*data_priv->switchIrqGlobal = 0x80;
	//leds ON
	*data_priv->ledGlobal = (unsigned short)0x0015;
}


void vexpress_set_led(int numLed,unsigned short *ledGlobal,enum led_brightness brightness){

	switch(numLed){

		case 0:
			if(brightness == LED_ON){
				*ledGlobal |= 0x1;
			}else if(brightness == LED_OFF){
				*ledGlobal &= ~(uint64_t)0x1;
			}
			break;

		case 1:
			if(brightness == LED_ON){
				*ledGlobal |= 0x2;
			}else if(brightness == LED_OFF){
				*ledGlobal &= ~(uint64_t)0x2;
			}
			break;

		case 2:
			if(brightness == LED_ON){
				*ledGlobal |= 0x4;
			}else if(brightness == LED_OFF){
				*ledGlobal &= ~(uint64_t)0x4;
			}
			break;

		case 3:
			if(brightness == LED_ON){
				*ledGlobal |= 0x8;
			}else if(brightness == LED_OFF){
				*ledGlobal &= ~(uint64_t)0x8;
			}
			break;

		case 4:
			if(brightness == LED_ON){
				*ledGlobal |= 0x10;
			}else if(brightness == LED_OFF){
				*ledGlobal &= ~(uint64_t)0x10;
			}
			break;
	}

}


/*################################################################################*/

void joystick_handler(struct platform_device *pdev, int key){

	vext_priv_t *data_priv = (vext_priv_t*) platform_get_drvdata(pdev);

	//key traduction
	switch(key){
		case UP:
			key = KEY_UP;
			break;
		case DOWN:
			key = KEY_DOWN;
			break;
		case RIGHT:
			key = KEY_RIGHT;
			break;
		case LEFT:
			key = KEY_LEFT;
			break;
		case CENTER:
			key = KEY_ENTER;
			break;
	}

	input_report_key(data_priv->input,key,1);
	input_sync(data_priv->input);

	input_report_key(data_priv->input,key,0);
	input_sync(data_priv->input);
}

void led_set(struct led_classdev* led_dev,enum led_brightness brightness) {

	vext_led_t *vext_led = container_of(led_dev,vext_led_t, led_cdev);
	struct platform_device *pdev = container_of(vext_led->led_cdev.dev->parent,struct platform_device, dev);
	vext_priv_t *data_priv = (vext_priv_t*) platform_get_drvdata(pdev);

	if(data_priv->plateforme == 0){
		vexpress_set_led(vext_led->nr_led,data_priv->ledGlobal,brightness);
	}
	else if (data_priv->plateforme == 1){
		display_led(vext_led->nr_led,brightness);
	}

}


static int vext_probe(struct platform_device *pdev) {


	int i;
	vext_priv_t *data_priv;
	const char* plateforme;
	struct device_node *dev_node;
	int ret;
	printk("init_start");
	data_priv = kzalloc(sizeof(vext_priv_t),GFP_KERNEL);
	BUG_ON(!data_priv);

	dev_node = of_find_node_by_name(NULL,"vext");
	BUG_ON(!dev_node);

	ret = of_property_read_string(dev_node,"plateforme",&plateforme);
	BUG_ON(ret);
	printk("platform %s",plateforme);

	if(!strcmp(plateforme, "vexpress")){
		data_priv->plateforme =0;
		init_vexpress(pdev,data_priv);

	}else if(!strcmp(plateforme, "rpi4")){
		data_priv->plateforme =1;
		rpisense_init();
		rpisense_joystick_handler_register(pdev,joystick_handler);

	}


	data_priv->input = input_allocate_device();

	data_priv->input->name = "vext_input";
	data_priv->input->dev.parent = &pdev->dev;

	for(i = 0; i < 5; i++){
		input_set_capability(data_priv->input,EV_KEY,keys[i]);
	}
	//event2 : major 13 minor 66
	ret = input_register_device(data_priv->input);




	for(i=0;i<5;i++){

		printk("init led %d set name",i);
		data_priv->leds[i].led_cdev.name = ledsName[i];

		printk("init led %d set call back",i);
		data_priv->leds[i].led_cdev.brightness_set = led_set;

		data_priv->leds[i].led_cdev.dev = kzalloc(sizeof(struct device),GFP_KERNEL);

		printk("init led %d set parent",i);
		data_priv->leds[i].led_cdev.dev->parent = &pdev->dev;

		printk("init led %d set num",i);
		data_priv->leds[i].nr_led = i;

		printk("init led %d led_classdev_register",i);
		led_classdev_register(&pdev->dev, &data_priv->leds[i].led_cdev);
	}
	platform_set_drvdata(pdev,data_priv);



	return 0;
}

static const struct of_device_id vext_of_ids[] = {
		{
				.compatible = "vexpress,vext",
		},
		{ /* sentinel */ },
};

static struct platform_driver vext_driver = {

		.probe = vext_probe,
		.driver = {
					.name = "vext",
					.of_match_table = vext_of_ids,
					.owner = THIS_MODULE,
				  },
};


static int access_init(void) {

	printk("access: hello!\n");
	platform_driver_register(&vext_driver);


	return 0;
}

static void access_exit(void) {


	printk("access: bye bye!\n");
}

module_init(access_init);
module_exit(access_exit);

MODULE_INFO(intree, "Y");
MODULE_LICENSE("GPL");

