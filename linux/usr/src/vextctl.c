#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/input.h>
#include <fcntl.h>

#define ON  "1"
#define OFF "0"

FILE *leds[5];
int event;


void setLedValue(FILE *led,char* value){
	fputs(value,led);
	fseek(led, 0, SEEK_SET);
}

void ledsOFF(){
	int i;
	for(i=0;i<5;i++){
		setLedValue(leds[i],OFF);
	}

}


int main(int argc, char **argv)
{

	int i;
	struct input_event ie;

	leds[0] = fopen("/sys/class/leds/vext_led0/brightness","w");
	leds[1] = fopen("/sys/class/leds/vext_led1/brightness","w");
	leds[2] = fopen("/sys/class/leds/vext_led2/brightness","w");
	leds[3] = fopen("/sys/class/leds/vext_led3/brightness","w");
	leds[4] = fopen("/sys/class/leds/vext_led4/brightness","w");
	event   = open("/dev/input/event1",O_RDONLY);

	if (event == -1){
		 printf("\nerror fopen\n");
				  return 0;
	}

	for(i=0;i<5;i++){
		if (leds[i] == NULL)
		{
		  printf("\nerror fopen\n");
		  return 0;
		}
	}



	while(1){

		read(event,&ie,sizeof(struct input_event));

		if(ie.value == 1 && ie.type == EV_KEY){

			switch(ie.code){
				case KEY_ENTER:
					ledsOFF();
					setLedValue(leds[0],ON);
					break;
				case KEY_LEFT:
					ledsOFF();
					setLedValue(leds[1],ON);
					break;
				case KEY_UP:
					ledsOFF();
					setLedValue(leds[2],ON);
					break;
				case KEY_RIGHT:
					ledsOFF();
					setLedValue(leds[3],ON);
					break;
				case KEY_DOWN:
					ledsOFF();
					setLedValue(leds[4],ON);
					break;
				}

		}
	}

	return 0;
}

