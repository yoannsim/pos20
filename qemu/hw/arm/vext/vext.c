/*
 * Copyright (C) 2013-2014 Romain Bornet <romain.bornet@heig-vd.ch>
 * Copyright (C) 2016-2020 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "vext.h"
#include "vext_emul.h"

//structur

typedef struct {
	SysBusDevice busdev;
	MemoryRegion iomem;
	qemu_irq     irq;
	uint16_t     REG[3];

} vext_stat_t;

vext_stat_t* globalStruct;


static void vext_write(void* opaque,hwaddr offset, uint64_t value,unsigned size ){
	vext_stat_t* s = (vext_stat_t*) opaque;
	cJSON *root = cJSON_CreateObject();

	/* Creation of the JSON object */
	if(offset == 0x3a){
		cJSON_AddStringToObject(root, "device", "led");
		cJSON_AddNumberToObject(root, "value", value);
		vext_cmd_post(root);

		//save led value
		s->REG[2] = value;
	}


	if(offset == 0x18){

		//geston du bit 7
		s->REG[1] &= ~0x80;
		s->REG[1] |= 0x80 & value;

		//gestion du status et du clear
		s->REG[1] &= ~0x1;
		s->REG[1] &= ~0x10;

		//clear irq qemu
		qemu_set_irq(s->irq,0);

	}



}

static uint64_t vext_read(void* opaque,hwaddr offset,unsigned size ){
	vext_stat_t* s = (vext_stat_t*) opaque;

	if( offset == 0x12){
		return s->REG[0];
	}

	if( offset == 0x18){
		return s->REG[1];
	}

	/* Creation of the JSON object */
	if(offset == 0x3a){
		return s->REG[2];
	}
	return 0;

}


static const MemoryRegionOps vext_ops = {
	.read = vext_read,
	.write = vext_write,
	.endianness = DEVICE_NATIVE_ENDIAN,

};

void vext_process_switch(cJSON *packet)
{
	char *dev = cJSON_GetObjectItem(packet,"device")->valuestring;
	int swStatus = cJSON_GetObjectItem(packet, "status")->valueint;
	int numSwitch = 0;



	if (!strcmp(dev, "switch")){

		globalStruct->REG[0] = swStatus;

		switch (swStatus){
			case 0x1:
				numSwitch = 0;
				break;
			case 0x2:
				numSwitch= 1;
				break;
			case 0x4:
				numSwitch = 2;
				break;
			case 0x8:
				numSwitch = 3;
				break;
			case 0x10:
				numSwitch = 4;
				break;
			case 0x20:
				numSwitch = 5;
				break;
			case 0x40:
				numSwitch = 6;
				break;
			case 0x80:
				numSwitch = 7;
				break;
		}

		//si les inter sont activé et que le registre clear vaut 0
		if(((globalStruct->REG[1] & 0x1) == 0) && (globalStruct->REG[1] & 0x80)){

			//mettre le registre clear à 1
			globalStruct->REG[1] |= 0x1;

			//set le numero du switch
			globalStruct->REG[1] &= ~0xE;
			globalStruct->REG[1] |= (numSwitch) << 1;

			//set le bit IRQ_STATUS
			globalStruct->REG[1] |= 0x10;


			//lancer l' inter dans qemu
			//qemu_set_irq(globalStruct->irq,1);
			qemu_irq_raise(globalStruct->irq);


		}
	}

	cJSON_Delete(packet);
}



static void vext_init(Object *obj)
{
	/* to be completed ... */
	DeviceState *dev = DEVICE(obj);
	SysBusDevice *sdb = SYS_BUS_DEVICE(obj);
	vext_stat_t *state = OBJECT_CHECK(vext_stat_t,dev,"vext");

	memory_region_init_io(&state->iomem,obj,&vext_ops,state,"vext",0x100);

	sysbus_init_mmio(sdb,&state->iomem);
	sysbus_init_irq(sdb,&state->irq);

	globalStruct = state;

	globalStruct->REG[0] = 0;
	globalStruct->REG[1] = 0x00;

	vext_emul_init();
}

static const TypeInfo vext_info = {
		.name  = "vext",
		.parent = TYPE_SYS_BUS_DEVICE,
		.instance_size = sizeof(vext_stat_t),
		.instance_init = vext_init,
};

static void vext_register_types(void){
	type_register_static(&vext_info);
}




type_init(vext_register_types)


