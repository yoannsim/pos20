/*
 * Copyright (C) 2016-2018 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* Simple of example of devclass device */

#include <vfs.h>
#include <memory.h>
#include <heap.h>
#include <string.h>
#include <printk.h>
#include <completion.h>
#include <device/irq.h>
#include <uapi/linux/input.h>
#include <device/driver.h>
#include <device/fdt.h>
#include <device/arch/rpisense.h>

#define LED_OFFSET 0x3a
#define SWITCH_OFFSET 0x12
#define SWITCH_IRQ_OFFET 0x18
#define RPI4 1
#define VEXPR 0
#define LED_ON  1
#define LED_OFF 0
#define VEXPR  0

static int keys[] = {KEY_ENTER,KEY_LEFT,KEY_UP,KEY_RIGHT,KEY_DOWN};

typedef struct{
	int plateforme;
	unsigned short *regSwitch;
	unsigned short *switchIrqGlobal;
	unsigned short *ledGlobal;
	completion_t compl;
	unsigned lastKey;
	int valKey;

} vext_priv_t;

int led_off(dev_t *dev){
	rpisense_matrix_off();
	return 0;
}

irq_return_t vext_IRQ_handler(int irq, void* dev_id){


	vext_priv_t *data_priv = (vext_priv_t*) dev_id;

	printk("interrupt number %d ",irq);


	//clear irq
	*data_priv->switchIrqGlobal = 0x80;

	return IRQ_BOTTOM;

}


irq_return_t vext_IRQ_thread(int irq, void* dev_id){



	vext_priv_t *data_priv = (vext_priv_t*) dev_id;

	int numSwitch = (*data_priv->switchIrqGlobal >> 1) & 0x7;


	if(*data_priv->regSwitch & ((unsigned short) 0x1 << numSwitch)){
		printk("interrupt appui  swich number %d ",numSwitch);
		data_priv->lastKey = numSwitch;
		data_priv->valKey = 1;
		complete(&data_priv->compl);
	}
	else{
		data_priv->valKey = 0;
		complete(&data_priv->compl);
	}
	return IRQ_COMPLETED;
}


void joystick_handler(void *priv, int key){

	vext_priv_t *data_priv = (vext_priv_t*) priv;

	//key traduction
	switch(key){
		case UP:
			key = KEY_UP;
			break;
		case DOWN:
			key = KEY_DOWN;
			break;
		case RIGHT:
			key = KEY_RIGHT;
			break;
		case LEFT:
			key = KEY_LEFT;
			break;
		case CENTER:
			key = KEY_ENTER;
			break;
	}

	data_priv->lastKey = key;
	data_priv->valKey = 1;

	complete(&data_priv->compl);

}


static int vexpresse_write(int fd, const void *buffer, int count) {
	struct devclass *dev;
	vext_priv_t* priv;
	char *bufferC = (char *)buffer;
	int id;
	int brightness = 1;

    dev = (struct devclass *) devclass_by_fd(fd);
    priv = (vext_priv_t*) devclass_get_priv(dev);
    id = devclass_fd_to_id(fd);

    if(!strcmp("0",bufferC)){
    	brightness = LED_OFF;
    }else if(!strcmp("1",bufferC)){
    	brightness = LED_ON;
    }

    printk("vext:write  %s  led%d \n",id,bufferC);

    switch(id){

    		case 0:
    			if(brightness == LED_ON){
    				*priv->ledGlobal |= 0x1;
    			}else if(brightness == LED_OFF){
    				*priv->ledGlobal &= ~(uint64_t)0x1;
    			}
    			break;

    		case 1:
    			if(brightness == LED_ON){
    				*priv->ledGlobal |= 0x2;
    			}else if(brightness == LED_OFF){
    				*priv->ledGlobal &= ~(uint64_t)0x2;
    			}
    			break;

    		case 2:
    			if(brightness == LED_ON){
    				*priv->ledGlobal |= 0x4;
    			}else if(brightness == LED_OFF){
    				*priv->ledGlobal &= ~(uint64_t)0x4;
    			}
    			break;

    		case 3:
    			if(brightness == LED_ON){
    				*priv->ledGlobal |= 0x8;
    			}else if(brightness == LED_OFF){
    				*priv->ledGlobal &= ~(uint64_t)0x8;
    			}
    			break;

    		case 4:
    			if(brightness == LED_ON){
    				*priv->ledGlobal|= 0x10;
    			}else if(brightness == LED_OFF){
    				*priv->ledGlobal &= ~(uint64_t)0x10;
    			}
    			break;
    	}



	return count;
}


static int rpi4_write(int fd, const void *buffer, int count) {
	struct devclass *dev;
	vext_priv_t* priv;
	char *bufferC = (char *)buffer;
	int id;
	int brightness = 1;

    dev = (struct devclass *) devclass_by_fd(fd);
    priv = (vext_priv_t*) devclass_get_priv(dev);
    id = devclass_fd_to_id(fd);
    bufferC[1] = 0;
    if(!strcmp("0",bufferC)){
    	brightness = LED_OFF;
    }else if(!strcmp("1",bufferC)){
    	brightness = LED_ON;
    }

    printk("vext:write  %s  led%d \n",bufferC,id);

    display_led(id,brightness);


	return count;
}


static int vexpresse_read(int fd, void *buffer, int count) {
	struct devclass *dev;
	vext_priv_t* priv;
	struct input_event ev;

	dev = (struct devclass *) devclass_by_fd(fd);
	priv = (vext_priv_t*) devclass_get_priv(dev);

	wait_for_completion(&priv->compl);

	ev.type = EV_KEY;
	ev.code = keys[priv->lastKey];
	ev.value= priv->valKey;

	if(count == sizeof(ev)){
		memcpy(buffer,(void*)&ev,count);
		return count;
	}else{
		return -1;
	}

}

static int rpi4_read(int fd, void *buffer, int count) {
	struct devclass *dev;
	vext_priv_t* priv;
	struct input_event ev;

	dev = (struct devclass *) devclass_by_fd(fd);
	priv = (vext_priv_t*) devclass_get_priv(dev);

	wait_for_completion(&priv->compl);

	ev.type = EV_KEY;
	ev.code = priv->lastKey;
	ev.value= priv->valKey;

	if(count == sizeof(ev)){
		memcpy(buffer,(void*)&ev,count);
		return count;
	}else{
		return -1;
	}
}


struct file_operations rpi4_fops = {
	.write = rpi4_write,
	.read =  rpi4_read
};

struct file_operations vexpresse_fops = {
	.write = vexpresse_write,
	.read =  vexpresse_read
};

struct devclass vexpresse_leds = {
	.class = "led",
	.id_start = 0,
	.id_end = 4,
	.type = VFS_TYPE_DEV_CHAR,
	.fops = &vexpresse_fops,
};
struct devclass rpi4_leds = {
	.class = "led",
	.id_start = 0,
	.id_end = 4,
	.type = VFS_TYPE_DEV_CHAR,
	.fops = &rpi4_fops,
};

struct devclass rpi4_switch = {
	.class = "switch",
	.type = VFS_TYPE_DEV_CHAR,
	.fops = &rpi4_fops,
};

struct devclass vext_switchs = {
	.class = "switch",
	.type = VFS_TYPE_DEV_CHAR,
	.fops = &vexpresse_fops,
};

int vext_init(dev_t *dev) {

    vext_priv_t *data_priv;
    uint32_t platf =0;
    uint32_t offset_of_vext;
    uint32_t offset_in_vext;
    uint32_t ledOff;
    uint32_t switchOff;
    uint32_t switchIrqOff;

    printk("\nvext init start\n");


	data_priv = (vext_priv_t*) malloc(sizeof(vext_priv_t));
	init_completion(&data_priv->compl);


	offset_of_vext = fdt_find_node_by_name(0,"vext");

	//offset des switchs
	fdt_property_read_u32(offset_of_vext,"plateforme",&platf);



	if(platf == VEXPR){
		printk("platform = vexpresse\n");
		devclass_set_priv(&vexpresse_leds,data_priv);
		devclass_set_priv(&vext_switchs,data_priv);


		/* Register the vext led driver so it can be accessed from user space. */
		devclass_register(dev, &vexpresse_leds);

		/* Register the vext led driver so it can be accessed from user space. */
		devclass_register(dev, &vext_switchs);


		irq_bind(dev->irq_nr,vext_IRQ_handler,vext_IRQ_thread,(void*) data_priv);

		printk("\ninterrupt number %d ",dev->irq_nr);


		//offset des switchs
		offset_in_vext = fdt_find_node_by_name(offset_of_vext,"switchs");
		fdt_property_read_u32(offset_in_vext,"reg",&switchOff);
		printk("\noffset switchOff %x \n",switchOff);

		//offset des switch_irq
		offset_in_vext = fdt_find_node_by_name(offset_of_vext,"switch_irq");
		fdt_property_read_u32(offset_in_vext,"reg",&switchIrqOff);
		printk("\noffset switchIrqOff %x \n",switchIrqOff);

		//offset des leds
		offset_in_vext = fdt_find_node_by_name(offset_of_vext,"led");
		fdt_property_read_u32(offset_in_vext,"reg",&ledOff);
		printk("\noffset led %x \n",ledOff);


		//map de la mem
		data_priv->ledGlobal = (unsigned short*) (dev->base + ledOff);
		data_priv->switchIrqGlobal = (unsigned short*) (dev->base + switchIrqOff);
		data_priv->regSwitch = (unsigned short*) (dev->base  + switchOff);



		//enable IRQ
		*data_priv->switchIrqGlobal = 0x80;
	}else if(platf == RPI4){
		printk("platform = rpi4\n");

		devclass_set_priv(&rpi4_leds,data_priv);
		devclass_set_priv(&rpi4_switch,data_priv);

		/* Register the vext led driver so it can be accessed from user space. */
		devclass_register(dev, &rpi4_leds);
		devclass_register(dev, &rpi4_switch);

		rpisense_joystick_handler_register((void*)data_priv,joystick_handler);

	}



	return 0;
}


REGISTER_DRIVER_POSTCORE("arm,vext", vext_init);
REGISTER_POSTINIT("arm,vext",led_off);
