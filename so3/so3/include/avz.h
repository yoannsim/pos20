#ifndef AVZ_H
#define AVZ_H

#define __HYPERVISOR_set_callbacks         0
#define __HYPERVISOR_console_io            1
#define __HYPERVISOR_physdev_op            2


/*
 * Commands to HYPERVISOR_console_io().
 */
#define CONSOLEIO_write_string  0
#define CONSOLEIO_process_char  1

extern int hypercall_trampoline(int hcall, long a0, long a2, long a3, long a4);

/*
 * start_info structure
 */

struct start_info {

    int	domID;
    unsigned long nr_pages;     /* Total pages allocated to this domain.  */

    void *shared_info;  /* AVZ virtual address of the shared info page */

    unsigned long hypercall_addr; /* Hypercall vector addr for direct branching without syscall */
    unsigned long fdt_paddr;

    /* Low-level print function mainly for debugging purpose */
    void (*printch)(char c);

    unsigned long store_mfn;    /* MACHINE page number of shared page.    */

    unsigned long pt_base;      /* VIRTUAL address of page directory.     */
    unsigned long nr_pt_frames; /* Number of bootstrap p.t. frames.       */

    unsigned long min_mfn;

    unsigned long logbool_ht_set_addr;  /* Address of the logbool ht_set function which can be used in the domain. */

};
typedef struct start_info start_info_t;

#endif /* AVZ_H */
