#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/input.h>
#include <fcntl.h>

#define ON  "1"
#define OFF "0"

FILE *leds[5];
int event;
int i;


void setLedValue(FILE *led,char* value){
	fputs(value,led);
	fseek(led, 0, SEEK_SET);
}

void ledsOFF(){
	int i;
	for(i=0;i<5;i++){
		setLedValue(leds[i],OFF);
	}

}


int main(int argc, char **argv)
{
	struct input_event ie;

	leds[0] = fopen("/dev/led0","w");
	leds[1] = fopen("/dev/led1","w");
	leds[2] = fopen("/dev/led2","w");
	leds[3] = fopen("/dev/led3","w");
	leds[4] = fopen("/dev/led4","w");
	event   = open("/dev/switch",0);

	if (event == -1){
		 printf("\nerror open\n");
				  return 0;
	}

	for(i=0;i<5;i++){
		if (leds[i] == NULL)
		{
		  printf("\nerror fopen\n");
		  return 0;
		}
	}



	while(1){

		if(read(event,&ie,sizeof(struct input_event)) == -1){
			return 0;
		}

		if(ie.type == EV_KEY){

			switch(ie.code){
				case KEY_ENTER:
					ledsOFF();
					setLedValue(leds[0],ON);
					break;
				case KEY_LEFT:
					ledsOFF();
					setLedValue(leds[1],ON);
					break;
				case KEY_UP:
					ledsOFF();
					setLedValue(leds[2],ON);
					break;
				case KEY_RIGHT:
					ledsOFF();
					setLedValue(leds[3],ON);
					break;
				case KEY_DOWN:
					ledsOFF();
					setLedValue(leds[4],ON);
					break;
			}
		}

	}

	return 0;
}
