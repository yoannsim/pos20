// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2000-2009
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 */

#include <common.h>
#include <command.h>
#include <version.h>
#include <linux/compiler.h>
#ifdef CONFIG_SYS_COREBOOT
#include <asm/arch/sysinfo.h>
#endif



static int do_hello(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{

	printf("hello\n");
	return 0;

}

U_BOOT_CMD(
	hello,	1,		1,	do_hello,
	"print hello",
	""
);
