#!/bin/bash
 
echo Installing new SenseHat GUI frontend and server ...
sudo apt install curl
curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py
sudo python3 get-pip.py
sudo pip install --upgrade pip
cd sense-hat
sudo python3 -m pip install sense_emu-1.1-py3-none-any.whl
cd ..

 
